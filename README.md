# subX

Subdomain discovery tool

```
		   _   __  __
 ___ _   _| |__\ \/ /
/ __| | | | '_ \\  /
\__ \ |_| | |_) /  \
|___/\__,_|_.__/_/\_\

        MetiSec - Version 1.0
usage: subx [-h] [-d DOMAIN] [-dl DOMAINLIST] [-dy] [-st] [-p] [-w WORDLIST] [-r RESOLVER] [-wm] [-silent] [-o OUTPUT] [-version]

0

options:
  -h, --help      show this help message and exit
  -d DOMAIN       Domain for finding all subdomains
  -dl DOMAINLIST  Domains list for finding all subdomains
  -dy             Only dynamic DNS brute force
  -st             Only static DNS brute force
  -p              Only use public tool, subfinder and assetfinder
  -w WORDLIST     Wordlist for DNS brute forcing
  -r RESOLVER     Resolver list
  -wm             making wordlist from outputn program
  -silent         Only show subdomains
  -o OUTPUT       Save output to file
  -version        show program's version number and exit
```
